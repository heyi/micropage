import axios from 'axios'

export function listStorage(query) {
  // create an axios instance
  const service = axios.create({
    baseURL: process.env.VUE_APP_BASE_API, // api 的 base_url
    timeout: 5000 // request timeout
  })
  return service({
    url: '/storage/list',
    method: 'get',
    params: query
  })
}
